<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
  <title>Simple Med Form App</title>
</head>
<body>

<h2>Patient Information</h2>
<form:form method = "POST" action = "/SimpleMedFormApp/addPatient">
  <table>
    <tr>
      <td><form:label path = "patname">Patient Name</form:label></td>
      <td><form:input path = "patname" /></td>
    </tr>
    <tr>
      <td><form:hidden path="patid" value="<%= (int) (Math.random() * 100)%>"/></td>
    </tr>
    <tr>
      <td><form:label path = "age">Age Category</form:label></td>
      <td><form:select path = "age">
        <form:option value = "NONE" label="Select"/>
        <form:options items="${ageList}"/>
      </form:select>
      </td>
    </tr>
    <tr>
      <td><form:label path = "address">Address</form:label></td>
      <td><form:textarea path = "address" rows = "5" cols = "30" /></td>
    </tr>
    <tr>
      <td><form:label path = "receiveUpdate">Subscribe Updates</form:label></td>
      <td><form:checkbox path = "receiveUpdate" /></td>
    </tr>
    <tr>
      <td><form:label path="gender">Gender</form:label></td>
      <td>
        <form:radiobutton path="gender" value="M" label="Male" />
        <form:radiobutton path="gender" value="F" label="Female"/>
        <form:radiobutton path="gender" value="T" label="Transgender"/>
      </td>
    </tr>
    <tr>
      <td><form:label path = "nursing">Nursing Category</form:label></td>
      <td><form:select path = "nursing">
        <form:option value = "NONE" label="Select"/>
        <form:options items="${nursingList}"/>
      </form:select>
      </td>
    </tr>
    <tr>
      <td><form:label path="insurance">Health Insurance</form:label></td>
      <td><form:select path="insurance">
        <form:option value="NONE" label="Select"/>
        <form:options items="${insuranceList}"/>
      </form:select>
      </td>
    </tr>
    <tr>
      <td colspan = "2">
        <input type = "submit" value = "Submit"/>
      </td>
    </tr>
  </table>
</form:form>
</body>
</html>