<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
    <title>Simple Med Form App</title>
</head>
<body>

<h2>Submitted Patient Information</h2>
<table>
    <tr>
        <td>Patient Name</td>
        <td>${patname}</td>
    </tr>
    <tr>
        <td>Patient ID</td>
        <td>${patid}</td>
    </tr>
    <tr>
        <td>Age Category</td>
        <td>${age}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>${address}</td>
    </tr>
    <tr>
        <td>Subscribed to Update</td>
        <td>${receiveUpdate}</td>
    </tr>
    <tr>
        <td>Gender</td>
        <td>${(gender=="M" ? "Male" : "Female" || gender=="F" ? "Female": "Transgender")}</td>
    </tr>
    <tr>
        <td>Nursing Category</td>
        <td>${nursing}</td>
    </tr>
    <tr>
        <td>Health Insurance</td>
        <td>${insurance}</td>
    </tr>
</table>
</body>
</html>