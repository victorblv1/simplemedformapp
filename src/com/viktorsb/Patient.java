package com.viktorsb;

public class Patient {

    private String patname;
    private Integer patid;
    private String age;
    private String gender;
    private String address;
    private boolean receiveUpdate; // currently for a demonstrative purposes
    private String nursing;
    private String insurance;

    public String getPatname() {
        return patname;
    }

    public void setPatname(String patname) {
        this.patname = patname;
    }

    public Integer getPatid() {
        return patid;
    }

    public void setPatid(Integer patid) {
        this.patid = patid;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String addrress) {
        this.address = addrress;
    }

    public boolean isReceiveUpdate() {
        return receiveUpdate;
    }

    public void setReceiveUpdate(boolean receiveUpdate) {
        this.receiveUpdate = receiveUpdate;
    }

    public String getNursing() {
        return nursing;
    }

    public void setNursing(String nursing) {
        this.nursing = nursing;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }
}
