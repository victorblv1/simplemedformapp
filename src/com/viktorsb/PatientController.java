package com.viktorsb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class PatientController {

    @RequestMapping(value = "/patient", method = RequestMethod.GET)
    public ModelAndView patient() {
        Patient patient = new Patient();
        patient.setGender("M");
        ModelAndView modelAndView = new ModelAndView("patient", "command", patient);
        return modelAndView;
    }

    @RequestMapping(value = "/addPatient", method = RequestMethod.POST)
    public String addPatien(@ModelAttribute("SimpleMedFormApp") Patient patient,
                            ModelMap model) {
        model.addAttribute("patname", patient.getPatname());
        model.addAttribute("patid", patient.getPatid());
        model.addAttribute("age", patient.getAge());
        model.addAttribute("gender", patient.getGender());
        model.addAttribute("address", patient.getAddress());
        model.addAttribute("receiveUpdate", patient.isReceiveUpdate());
        model.addAttribute("nursing", patient.getNursing());
        model.addAttribute("insurance", patient.getInsurance());
        return "patients";
    }

    @ModelAttribute("ageList")
    public Map<String, String> getAgeList() {
        Map<String, String> ageList = new HashMap<String, String>();
        ageList.put("0 - 15", "0 - 15");
        ageList.put("15 - 50", "15 - 50");
        ageList.put("50+", "50+");
        return ageList;
    }

    @ModelAttribute("nursingList")
    public Map<String, String> getNursingList() {
        Map<String, String> nursingList = new HashMap<String, String>();
        nursingList.put("Stationary", "Stationary");
        nursingList.put("General Practitioner", "General Practitioner");
        nursingList.put("Home Nursing", "Home Nursing");
        nursingList.put("Self-treated", "Self-treated");
        return nursingList;
    }

    @ModelAttribute("insuranceList")
    public Map<String, String> getInsuranceList() {
        Map<String, String> insuranceList = new HashMap<String, String>();
        insuranceList.put("Public", "Public");
        insuranceList.put("Private", "Private");
        insuranceList.put("Self-paid", "Self-paid");
        insuranceList.put("Uninsured", "Uninsured");
        return insuranceList;
    }
}
